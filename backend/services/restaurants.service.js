const fs = require('fs');
const Restaurant = require('../models/Restaurant');
const { ROLES } = require('../utils/enums');

const getRestaurantById = async (id, user) => {
  const query = { _id: id };
  if (user.role === ROLES.OWNER) {
    query.user = user.id;
  }
  return Restaurant.findOne(query).populate([
    { path: 'user', select: 'fullName email' },
    {
      path: 'reviews',
      select: '-restaurant',
      options: { sort: { createdAt: -1 } },
      populate: { path: 'user', select: '-password' },
    },
  ]);
};

const getRestaurants = async (user, { sortBy, sortType, rateMax, rateMin }) => {
  const query = {};
  const sort = {};
  if (user.role === ROLES.OWNER) {
    query.user = user.id;
  }
  if (sortBy === 'date') {
    sort.createdAt = sortType === 'asc' ? 1 : -1;
  }
  let data = (
    await Restaurant.find(query)
      .sort(sort)
      .populate([
        { path: 'user', select: 'fullName email' },
        { path: 'reviews', select: '-restaurant' },
      ])
  ).map(value => value.toClient());

  if (sortBy === 'rating') {
    const sortFlow = sortType === 'asc' ? 1 : -1;
    data.sort((a, b) =>
      a.averageRate > b.averageRate
        ? sortFlow
        : b.averageRate > a.averageRate
          ? -1 * sortFlow
          : 0);
  }

  if (rateMax || rateMin) {
    const min = Number(rateMin) || 0;
    const max = Number(rateMax) || 6;
    data = data.filter(
      value =>
        Number(value.averageRate) <= max && Number(value.averageRate) >= min,
    );
  }
  return data;
};

const addRestaurant = async ({ title, description, address, cover, user }) => {
  const restaurant = new Restaurant({
    title,
    description,
    address,
    cover,
    user,
  });

  await restaurant.save();

  return restaurant;
};

const updateRestaurant = async (id, values, user) => {
  const query = { _id: id };
  if (user.role === ROLES.OWNER) {
    query.user = user.id;
  }
  const restaurant = await Restaurant.findOne(query);
  if (!restaurant) return null;
  Object.keys(values).forEach(key => {
    restaurant[`${key}`] = values[`${key}`];
  });
  await restaurant.save();

  return restaurant;
};

const findRestaurant = async (id, values, user) => {
  const query = { _id: id };
  if (user.role === ROLES.OWNER) {
    query.user = user.id;
  }
  const restaurant = await Restaurant.findOne(query);
  if (!restaurant) return null;

  return restaurant;
};

const deleteRestaurant = async (id, user) => {
  const query = { _id: id };
  if (user.role === ROLES.OWNER) {
    query.user = user.id;
  }
  const restaurant = await Restaurant.findOne(query);
  if (!restaurant) return null;
  if (restaurant.cover) {
    await fs.promises.unlink(restaurant.cover);
  }
  await restaurant.remove();
  return true;
};

module.exports = {
  addRestaurant,
  getRestaurantById,
  getRestaurants,
  updateRestaurant,
  deleteRestaurant,
  findRestaurant,
};
