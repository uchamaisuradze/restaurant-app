const Review = require('../models/Review');
const { ROLES } = require('../utils/enums');

const getReviewById = async (id, user) => {
  const query = { _id: id };
  const populateRestaurant = { path: 'restaurant', select: '-reviews' };
  if (user.role === ROLES.OWNER) {
    populateRestaurant.match = { user: user.id };
  }
  return Review.findOne(query).populate([
    { path: 'user', select: 'fullName email' },
    populateRestaurant,
  ]);
};

const getReviews = async (restaurant = null, user, pending = false) => {
  const query = {};
  const populateRestaurant = { path: 'restaurant', select: '-reviews' };
  if (user.role === ROLES.OWNER) {
    populateRestaurant.match = { user: user.id };
  }
  if (restaurant) {
    query.restaurant = restaurant;
  }
  if (pending) {
    query.reply = null;
  }
  const reviews = await Review.find(query)
    .sort({ createdAt: -1 })
    .populate([{ path: 'user', select: 'fullName email' }, populateRestaurant]);
  return reviews ? reviews.filter(review => review.restaurant) : [];
};

const findReviews = async values => {
  const query = { ...values };
  return Review.find(query);
};

const addReview = async ({ comment, visitDate, rate, restaurant, user }) => {
  const review = new Review({
    comment,
    visitDate,
    rate,
    restaurant,
    user,
    reply: null,
  });

  await review.save();

  return review;
};

const updateReview = async (id, values) => {
  const query = { _id: id };
  const review = await Review.findOne(query);
  if (!review) return null;
  Object.keys(values).forEach(key => {
    review[`${key}`] = values[`${key}`];
  });
  await review.save();

  return review;
};

const deleteReview = async id => {
  const query = { _id: id };
  const review = await Review.findOne(query);
  if (!review) return null;
  await review.remove();
  return true;
};

module.exports = {
  addReview,
  getReviewById,
  getReviews,
  updateReview,
  deleteReview,
  findReviews,
};
