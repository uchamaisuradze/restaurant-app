const bcrypt = require('bcryptjs');
const User = require('../models/User');
const HttpError = require('../utils/http-error');

const getUserById = async id => User.findById(id, '-password');
const getAllUsers = async () => User.find({}, '-password');

const signUpUser = async ({ fullName, email, password, role }) => {
  const hashedPassword = await bcrypt.hash(password, await bcrypt.genSalt(10));

  const user = new User({
    fullName,
    email,
    password: hashedPassword,
    role,
  });

  await user.save();

  return user;
};

const updateUser = async (id, values) => {
  const user = await User.findById(id, '-password');
  if (!user) return null;
  Object.keys(values).forEach(key => {
    user[`${key}`] = values[`${key}`];
  });
  await user.save();

  return user;
};

const deleteUser = async id => {
  const user = await User.findById(id);
  if (!user) return null;
  await user.remove();
  return true;
};

const signInUser = async ({ email, password }) => {
  const user = await User.findOne({ email });
  if (!user) {
    throw new HttpError('Invalid credentials', 401);
  }

  const isMatch = await bcrypt.compare(password, user.password);
  if (!isMatch) {
    throw new HttpError('Invalid credentials', 401);
  }

  return user;
};

const checkOldPassword = async ({ id, oldPassword }) => {
  const user = await User.findById(id);
  if (!user) {
    return false;
  }

  const isMatch = await bcrypt.compare(oldPassword, user.password);
  return isMatch;
};

module.exports = {
  signUpUser,
  signInUser,
  getUserById,
  getAllUsers,
  updateUser,
  deleteUser,
  checkOldPassword,
};
