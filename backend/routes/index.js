const auth = require('./api/auth.routes');
const users = require('./api/users.routes');
const restaurants = require('./api/restaurants.routes');
const reviews = require('./api/reviews.routes');

module.exports = {
  auth,
  users,
  restaurants,
  reviews,
};
