const { Router } = require('express');
const {
  validateAddRestaurantInputs,
  validateUpdateRestaurantInputs,
  validateDeleteInputs,
  fileValidationHandler,
} = require('../../utils/validations');
const {
  addRestaurantController,
  getRestaurantsController,
  updateRestaurantController,
  deleteRestaurantController,
} = require('../../controllers/restaurants.controller');
const authMiddleware = require('../../middleware/auth.middleware');
const { ROLES } = require('../../utils/enums');

const router = Router();

// @route   POST api/restaurants
// @desc    Create Restaurant
// @access  Private
router.post(
  '/',
  authMiddleware(ROLES.OWNER),
  fileValidationHandler('image'),
  validateAddRestaurantInputs,
  addRestaurantController,
);

// @route   GET api/restaurants
// @desc    Get All Restaurants or by id
// @access  Private
router.get('/', authMiddleware(), getRestaurantsController);

// @route   PATCH api/restaurants
// @desc    Update any restaurant
// @access  Private
router.patch(
  '/',
  authMiddleware([ROLES.ADMIN, ROLES.OWNER]),
  fileValidationHandler('image'),
  validateUpdateRestaurantInputs,
  updateRestaurantController,
);

// @route   DELETE api/restaurants
// @desc    Delete any restaurant
// @access  Private
router.delete(
  '/',
  authMiddleware([ROLES.ADMIN]),
  validateDeleteInputs,
  deleteRestaurantController,
);

module.exports = router;
