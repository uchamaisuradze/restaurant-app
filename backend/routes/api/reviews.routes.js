const { Router } = require('express');
const {
  validateAddReviewInputs,
  validateUpdateReviewInputs,
  validateAddReplyInputs,
  validateDeleteInputs,
} = require('../../utils/validations');
const {
  addReviewController,
  getReviewsController,
  updateReviewController,
  deleteReviewController,
  addReplyToReviewController,
} = require('../../controllers/reviews.controller');
const authMiddleware = require('../../middleware/auth.middleware');
const { ROLES } = require('../../utils/enums');

const router = Router();

// @route   POST api/reviews
// @desc    Add Review
// @access  Private
router.post(
  '/',
  authMiddleware(ROLES.REGULAR),
  validateAddReviewInputs,
  addReviewController,
);

// @route   GET api/reviews
// @desc    Get All Reviews or by id
// @access  Private
router.get('/', authMiddleware(), getReviewsController);

// @route   PATCH api/reviews
// @desc    Update any review
// @access  Private
router.patch(
  '/',
  authMiddleware([ROLES.ADMIN]),
  validateUpdateReviewInputs,
  updateReviewController,
);

// @route   POST api/reviews/reply
// @desc    Add reply to review
// @access  Private
router.post(
  '/reply',
  authMiddleware([ROLES.OWNER]),
  validateAddReplyInputs,
  addReplyToReviewController,
);

// @route   DELETE api/reviews
// @desc    Delete any review
// @access  Private
router.delete(
  '/',
  authMiddleware([ROLES.ADMIN]),
  validateDeleteInputs,
  deleteReviewController,
);

module.exports = router;
