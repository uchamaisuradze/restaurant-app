const { Router } = require('express');
const {
  validateSignUpInputs,
  validateUpdateUserInputs,
  validateDeleteInputs,
} = require('../../utils/validations');
const {
  signUpUserController,
  getUsersController,
  updateUserController,
  deleteUserController,
} = require('../../controllers/users.controller');
const authMiddleware = require('../../middleware/auth.middleware');
const { ROLES } = require('../../utils/enums');

const router = Router();

// @route   POST api/users
// @desc    Create User
// @access  Public
router.post('/', validateSignUpInputs, signUpUserController);

// @route   GET api/users
// @desc    Get All Users or by id
// @access  Private
router.get('/', authMiddleware([ROLES.ADMIN]), getUsersController);

// @route   PATCH api/users
// @desc    Update any user
// @access  Private
router.patch(
  '/',
  authMiddleware([ROLES.ADMIN]),
  validateUpdateUserInputs,
  updateUserController,
);

// @route   DELETE api/users
// @desc    Delete any user
// @access  Private
router.delete(
  '/',
  authMiddleware([ROLES.ADMIN]),
  validateDeleteInputs,
  deleteUserController,
);

module.exports = router;
