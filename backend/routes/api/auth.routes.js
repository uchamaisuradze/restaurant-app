const { Router } = require('express');
const { validateSignInInputs, validateUpdateAuthUserInputs } = require('../../utils/validations');
const {
  signInAuthController,
  getUserAuthController,
  updateUserAuthController,
} = require('../../controllers/auth.controller');
const authMiddleware = require('../../middleware/auth.middleware');

const router = Router();

// @route   POST api/auth
// @desc    Authenticate user & get token
// @access  Public
router.post('/', validateSignInInputs, signInAuthController);

// @route   GET api/auth
// @desc    Get authenticated user
// @access  Private
router.get('/', authMiddleware(), getUserAuthController);

// @route   PATCH api/auth
// @desc    Update authenticated user
// @access  Private
router.patch('/', authMiddleware(), validateUpdateAuthUserInputs, updateUserAuthController);

module.exports = router;
