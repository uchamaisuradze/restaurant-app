const jwt = require('jsonwebtoken');
const { errorHandler, HttpError } = require('../utils');
const { APP } = require('../config');

module.exports = (rolesArr = []) => (req, res, next) => {
  const token = req.header('Authorization');

  if (!token) {
    return errorHandler(res, new HttpError('No token, authorization denied', 401));
  }

  try {
    const { user } = jwt.verify(token.split('Bearer ')[1], APP.JWT_SECRET);
    req.user = user;
    if (!!rolesArr.length && !rolesArr.includes(user.role)) {
      return errorHandler(res, new HttpError('Permission denied', 403));
    }
    next();
  } catch (err) {
    errorHandler(res, new HttpError('Token is not valid', 401));
  }
};
