module.exports = {
  DB: {
    USERNAME: 'ucha_m',
    PASSWORD: '123asdASD',
    HOST: 'cluster0.kwaqc.mongodb.net',
    DATABASE: 'restaurant_app',
    PARAMETERS: 'retryWrites=true&w=majority',
  },
  APP: {
    PORT: process.env.PORT || 5000,
    JWT_SECRET: '638F35396E6F02EE2206A8EA73174A187758ED04E5F2D31FE2C458811668E277',
    JWT_OPTIONS: { expiresIn: 360000 },
  },
};
