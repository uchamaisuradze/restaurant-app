const bcrypt = require('bcryptjs');
const {
  signUpUser,
  getAllUsers,
  getUserById,
  updateUser,
  deleteUser,
} = require('../services/users.service');
const { errorHandler, notFoundError, forbiddenError } = require('../utils');

const signUpUserController = async (req, res) => {
  const { fullName, email, password, role } = req.body;

  try {
    const user = await signUpUser({
      fullName: fullName.trim(),
      email: email.trim().toLowerCase(),
      password,
      role,
    });
    const token = await user.generateJwt();
    res.status(201).json({ token });
  } catch (err) {
    errorHandler(res, err);
  }
};

const getUsersController = async (req, res) => {
  try {
    const id = req.query?.id;
    let data;
    if (id) {
      data = await getUserById(id);
      if (!data) {
        return notFoundError(res);
      }
      data = data.toClient();
    } else {
      data = (await getAllUsers()).map(value => value.toClient());
    }
    res.status(200).json({ data });
  } catch (err) {
    errorHandler(res, err);
  }
};

const updateUserController = async (req, res) => {
  const { id, fullName, password, email } = req.body;

  try {
    const values = {
      fullName: fullName.trim(),
      email: email.trim().toLowerCase(),
    };
    if (password) {
      values.password = await bcrypt.hash(password, await bcrypt.genSalt(10));
    }
    const data = await updateUser(id, values);
    if (!data) {
      return notFoundError(res);
    }
    res.status(200).json({ data: data.toClient() });
  } catch (err) {
    errorHandler(res, err);
  }
};

const deleteUserController = async (req, res) => {
  const { id } = req.body;

  try {
    const data = await getUserById(id);
    if (!data) {
      return notFoundError(res);
    }
    if (data.id === req.user.id) {
      return forbiddenError(res, 'You cannot delete yourself.');
    }
    await deleteUser(id);
    res.status(204).send();
  } catch (err) {
    errorHandler(res, err);
  }
};

module.exports = {
  signUpUserController,
  getUsersController,
  updateUserController,
  deleteUserController,
};
