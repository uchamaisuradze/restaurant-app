const bcrypt = require('bcryptjs');
const { signInUser, getUserById, updateUser, checkOldPassword } = require('../services/users.service');
const { errorHandler, HttpError } = require('../utils');

const signInAuthController = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await signInUser({ email: email.trim().toLowerCase(), password });
    const token = await user.generateJwt();

    res.status(200).json({ token });
  } catch (err) {
    errorHandler(res, err);
  }
};

const getUserAuthController = async (req, res) => {
  const { id } = req.user;

  try {
    const user = await getUserById(id);
    res.status(200).json({ data: user.toClient() });
  } catch (err) {
    errorHandler(res, err);
  }
};

const updateUserAuthController = async (req, res) => {
  const { fullName, password, oldPassword } = req.body;
  const { id } = req.user;
  try {
    const values = {
      fullName: fullName.trim(),
    };
    if (!!password && !!oldPassword) {
      const checkPassword = await checkOldPassword({ id, oldPassword });
      if (!checkPassword) {
        return errorHandler(res, new HttpError('Old Password is incorrect', 401));
      }
      values.password = await bcrypt.hash(password, await bcrypt.genSalt(10));
    }
    const data = await updateUser(id, values);
    res.status(200).json({ data: data.toClient() });
  } catch (err) {
    errorHandler(res, err);
  }
};

module.exports = {
  signInAuthController,
  getUserAuthController,
  updateUserAuthController,
};
