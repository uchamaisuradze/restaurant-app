const fs = require('fs');
const {
  addRestaurant,
  updateRestaurant,
  getRestaurantById,
  getRestaurants,
  deleteRestaurant,
  findRestaurant,
} = require('../services/restaurants.service');
const { errorHandler, notFoundError } = require('../utils');

const addRestaurantController = async (req, res) => {
  const { title, description, address } = req.body;
  const user = req.user.id;
  try {
    const restaurant = await addRestaurant({
      title: title.trim(),
      description: description.trim(),
      cover: req.file?.path || null,
      address: address?.trim(),
      user,
    });
    res.status(201).json({ data: restaurant.toClient() });
  } catch (err) {
    if (req.file) {
      await fs.promises.unlink(req.file.path);
    }
    errorHandler(res, err);
  }
};

const getRestaurantsController = async (req, res) => {
  try {
    const id = req.query?.id;
    let data;
    if (id) {
      data = await getRestaurantById(id, req.user);
      if (!data) {
        return notFoundError(res);
      }
      data = data.toClient();
      data.canReview = false;
      if (req.user.role === 'regular') {
        const isReviewed = data.reviews.filter(review => review.user.id === req.user.id).length > 0;
        if (!isReviewed) {
          data.canReview = true;
        }
      }
      data.highestReview = null;
      data.lowestReview = null;
      let highest = 0;
      let lowest = 6;
      data.reviews.forEach(review => {
        if (review.rate > highest) {
          highest = review.rate;
          data.highestReview = { ...review };
        }
        if (review.rate < lowest) {
          lowest = review.rate;
          data.lowestReview = { ...review };
        }
      });
    } else {
      const { query } = req;
      data = await getRestaurants(req.user, query);
    }
    res.status(200).json({ data });
  } catch (err) {
    errorHandler(res, err);
  }
};

const updateRestaurantController = async (req, res) => {
  const { id, title, address, description } = req.body;

  try {
    const values = {
      title: title.trim(),
      description: description.trim(),
      address: address.trim(),
    };
    let removeFile = null;
    if (req.file) {
      values.cover = req.file?.path || null;
      const restaurant = await findRestaurant(id, values, req.user);
      if (restaurant && restaurant.cover) {
        removeFile = restaurant.cover;
      }
    }
    const data = await updateRestaurant(id, values, req.user);
    if (!data) {
      if (req.file) {
        await fs.promises.unlink(req.file.path);
      }
      return notFoundError(res);
    }
    if (removeFile) {
      await fs.promises.unlink(removeFile);
    }
    res.status(200).json({ data: data.toClient() });
  } catch (err) {
    if (req.file) {
      await fs.promises.unlink(req.file.path);
    }
    errorHandler(res, err);
  }
};

const deleteRestaurantController = async (req, res) => {
  const { id } = req.body;

  try {
    const data = await deleteRestaurant(id, req.user);
    if (!data) {
      return notFoundError(res);
    }
    res.status(204).send();
  } catch (err) {
    errorHandler(res, err);
  }
};

module.exports = {
  addRestaurantController,
  getRestaurantsController,
  updateRestaurantController,
  deleteRestaurantController,
};
