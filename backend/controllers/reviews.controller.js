const {
  addReview,
  updateReview,
  getReviewById,
  getReviews,
  deleteReview,
  findReviews,
} = require('../services/reviews.service');
const { errorHandler, notFoundError, forbiddenError, HttpError } = require('../utils');

const addReviewController = async (req, res) => {
  const {
    comment,
    visit_date: visitDate,
    rate,
    restaurant_id: restaurant,
  } = req.body;
  const user = req.user.id;
  const userAlreadyReviewed = await findReviews({ user, restaurant });

  if (userAlreadyReviewed && userAlreadyReviewed.length > 0) {
    return errorHandler(
      res,
      new HttpError('You have already reviewed this restaurant', 400),
    );
  }
  try {
    const review = await addReview({
      comment: comment.trim(),
      visitDate,
      rate,
      restaurant,
      user,
    });
    res.status(201).json({ data: review.toClient() });
  } catch (err) {
    errorHandler(res, err);
  }
};

const getReviewsController = async (req, res) => {
  try {
    const id = req.query?.id;
    let data;
    if (id) {
      data = await getReviewById(id, req.user);
      if (!data) {
        return notFoundError(res);
      }
      if (!data.restaurant) {
        return forbiddenError(res);
      }
      data = data.toClient();
    } else {
      const restaurant = req.query?.restaurant_id;
      const pending = typeof req.query?.pending !== 'undefined';
      data = (await getReviews(restaurant, req.user, pending)).map(value => value.toClient());
    }
    res.status(200).json({ data });
  } catch (err) {
    errorHandler(res, err);
  }
};

const updateReviewController = async (req, res) => {
  const {
    id,
    comment,
    visit_date: visitDate,
    rate,
    reply = null,
  } = req.body;

  try {
    const values = {
      comment: comment.trim(),
      visitDate,
      rate,
    };

    if (reply) {
      values.reply = reply;
    } else {
      values.reply = null;
    }
    const data = await updateReview(id, values);
    if (!data) {
      return notFoundError(res);
    }
    res.status(200).json({ data: data.toClient() });
  } catch (err) {
    errorHandler(res, err);
  }
};

const addReplyToReviewController = async (req, res) => {
  const {
    id,
    reply,
  } = req.body;

  try {
    const data = await getReviewById(id, req.user);
    if (!data) {
      return notFoundError(res);
    }
    if (!data.restaurant) {
      return forbiddenError(res);
    }
    if (data.reply) {
      return errorHandler(res, new HttpError('You have already replied to this review', 400));
    }
    const updatedData = await updateReview(id, { reply });
    res.status(201).json({ data: updatedData.toClient() });
  } catch (err) {
    errorHandler(res, err);
  }
};

const deleteReviewController = async (req, res) => {
  const { id } = req.body;

  try {
    const data = await deleteReview(id);
    if (!data) {
      return notFoundError(res);
    }
    res.status(204).send();
  } catch (err) {
    errorHandler(res, err);
  }
};

module.exports = {
  addReviewController,
  getReviewsController,
  updateReviewController,
  addReplyToReviewController,
  deleteReviewController,
};
