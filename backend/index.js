const express = require('express');
const path = require('path');
const cors = require('cors');
const { dbConnect } = require('./utils');
const { APP } = require('./config');
const { users, auth, restaurants, reviews } = require('./routes');

const app = express();
dbConnect();

app.use(cors());
app.use('/uploads/images', express.static(path.join('uploads', 'images')));
app.use(express.json());

app.use('/api/users', users);
app.use('/api/auth', auth);
app.use('/api/restaurants', restaurants);
app.use('/api/reviews', reviews);

const { PORT } = APP;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
