const mongoose = require('mongoose');
const { ROLES } = require('../utils/enums');
const { toClient, generateJwt } = require('../utils');

const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    fullName: {
      type: String,
      required: true,
    },
    avatar: {
      type: String,
    },
    role: {
      type: String,
      required: true,
      enum: [ROLES.ADMIN, ROLES.OWNER, ROLES.REGULAR],
    },
  },
  { timestamps: true },
);

UserSchema.post('remove', async (doc, next) => {
  const { _id: id } = doc;
  await doc.model('restaurant').deleteMany({ user: id });
  await doc.model('review').deleteMany({ user: id });
  next();
});

UserSchema.method('toClient', toClient);
UserSchema.method('generateJwt', generateJwt);

module.exports = mongoose.model('user', UserSchema);
