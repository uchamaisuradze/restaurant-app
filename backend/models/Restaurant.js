const mongoose = require('mongoose');
const Review = require('./Review');
const { toClient } = require('../utils');

const RestaurantSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    address: {
      type: String,
    },
    cover: {
      type: String,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'user',
    },
    reviews: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'review',
      },
    ],
  },
  { timestamps: true },
);

RestaurantSchema.post('remove', async (doc, next) => {
  const { _id: id } = doc;
  await Review.deleteMany({ restaurant: id });
  next();
});
// eslint-disable-next-line prefer-arrow-callback, func-names
RestaurantSchema.virtual('averageRate').get(function () {
  const { reviews } = this;
  let averageRate = typeof reviews === 'undefined' ? undefined : null;
  if (reviews && reviews.length > 0) {
    reviews.forEach(review => {
      const { rate } = review;
      if (rate) {
        averageRate += rate;
      }
    });
    if (averageRate > 0) {
      averageRate = (averageRate / reviews.length).toFixed(2);
    } else {
      averageRate = undefined;
    }
  }
  return averageRate;
});

RestaurantSchema.method('toClient', toClient);

module.exports = mongoose.model('restaurant', RestaurantSchema);
