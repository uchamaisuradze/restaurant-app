const mongoose = require('mongoose');
const { toClient } = require('../utils');

const ReviewSchema = new mongoose.Schema(
  {
    comment: {
      type: String,
      required: true,
    },
    visitDate: {
      type: Date,
      required: true,
    },
    rate: {
      type: Number,
      min: 1,
      max: 5,
      required: true,
    },
    reply: {
      type: String,
      default: null,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'user',
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'restaurant',
    },
  },
  { timestamps: true },
);

// eslint-disable-next-line func-names
ReviewSchema.pre('save', function (next) {
  this.wasNew = this.isNew;
  next();
});

// eslint-disable-next-line prefer-arrow-callback, func-names
ReviewSchema.post('save', async function (doc, next) {
  if (this.wasNew) {
    const { _id: id, restaurant: restaurantId } = doc;
    const relatedModel = await doc.model('restaurant').findById(restaurantId);
    relatedModel.reviews.push(id);
    await relatedModel.save();
  }
  next();
});

ReviewSchema.post('remove', async (doc, next) => {
  const { _id: id } = doc;
  const relatedModel = await doc.model('restaurant').findOne({ reviews: id });
  relatedModel.reviews.pull(id);
  await relatedModel.save();
  next();
});

ReviewSchema.method('toClient', toClient);

module.exports = mongoose.model('review', ReviewSchema);
