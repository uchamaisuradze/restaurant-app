const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const { DB } = require('../config');
const HttpError = require('./http-error');
const toClient = require('./to-client');
const { APP } = require('../config');

const dbConnect = () => {
  const connectionString = `mongodb+srv://${DB.USERNAME}:${DB.PASSWORD}@${DB.HOST}/${DB.DATABASE}?${DB.PARAMETERS}`;
  mongoose
    .connect(connectionString, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    })
    .then(() => console.log('MongoDB Connected...'))
    .catch(e => e);
};

const errorHandler = (res, err) => {
  if (err.code === 11000) {
    const errors = [];
    const keys = Object.keys(err.keyPattern);
    keys.forEach(key => {
      errors.push({ msg: `Provided ${key} already exists`, param: key });
    });
    res.status(400).send({ errors });
  } else if (err.kind === 'ObjectId') {
    res.status(404).send({ errors: [{ msg: 'No records found' }] });
  } else {
    res
      .status(err.statusCode || err.code || 500)
      .send({ errors: [{ msg: err.message || err }] });
  }
};

const notFoundError = res =>
  errorHandler(res, new HttpError('No records found', 404));
const forbiddenError = (res, text = 'Permission Denied') =>
  errorHandler(res, new HttpError(text, 403));

async function generateJwt() {
  const { id, fullName, role, avatar } = this;
  const payload = {
    user: {
      id,
      fullName,
      role,
      avatar,
    },
  };

  const { JWT_SECRET, JWT_OPTIONS } = APP;
  return jwt.sign(payload, JWT_SECRET, JWT_OPTIONS);
}

module.exports = {
  HttpError,
  toClient,
  dbConnect,
  errorHandler,
  notFoundError,
  forbiddenError,
  generateJwt,
};
