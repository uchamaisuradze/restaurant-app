/* eslint-disable no-underscore-dangle */
function toClient() {
  const obj = this.toJSON({ virtuals: true });
  delete obj._id;
  delete obj.__v;
  return obj;
}

module.exports = toClient;
