const { check, validationResult, body } = require('express-validator');
const fs = require('fs');
const { ROLES } = require('./enums');
const fileUpload = require('../middleware/file-upload.middleware');

const validationsHandler = () => async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    if (req.file) {
      await fs.promises.unlink(req.file.path);
    }
    return res.status(400).json({ errors: errors.array() });
  }
  next();
};

module.exports.fileValidationHandler = name => (req, res, next) => {
  const upload = fileUpload.single(name);
  upload(req, res, err => {
    if (err) {
      res.status(400).json({ errors: [{ msg: err.message }] });
    } else {
      next();
    }
  });
};

module.exports.validateSignUpInputs = [
  check('fullName', 'Full name is required').not().isEmpty(),
  check('email', 'Please include a valid email').isEmail(),
  check('password', 'Password length should be 6 or more characters').isLength({
    min: 6,
  }),
  check('role', 'Please specify role: Owner or Regular User').isIn([
    ROLES.OWNER,
    ROLES.REGULAR,
  ]),
  validationsHandler(),
];

module.exports.validateAddRestaurantInputs = [
  check('title', 'Title is required').not().isEmpty(),
  check('description', 'Description is required').not().isEmpty(),
  validationsHandler(),
];

module.exports.validateAddReviewInputs = [
  check('comment', 'Comment is required').not().isEmpty(),
  check('rate', 'Rate should be integer from 1 to 5').isInt({ gt: 0, lt: 6 }),
  check('visit_date', 'Provide valid visit date')
    .not()
    .isEmpty()
    .isDate()
    .isBefore(new Date().toString()),
  check('restaurant_id', 'Please provide Restaurant ID').not().isEmpty(),
  validationsHandler(),
];

module.exports.validateUpdateUserInputs = [
  check('id', 'Please provide ID').not().isEmpty(),
  check('fullName', 'Full name is required').not().isEmpty(),
  body('password', 'Password length should be 6 or more characters')
    .if(body('password').not().isEmpty())
    .isLength({
      min: 6,
    }),
  validationsHandler(),
];

module.exports.validateUpdateRestaurantInputs = [
  check('id', 'Please provide ID').not().isEmpty(),
  check('title', 'Title is required').not().isEmpty(),
  check('description', 'Description is required').not().isEmpty(),
  validationsHandler(),
];

module.exports.validateUpdateReviewInputs = [
  check('id', 'Please provide ID').not().isEmpty(),
  check('comment', 'Comment is required').not().isEmpty(),
  check('rate', 'Rate should integer from 1 to 5').isInt({ gt: 0, lt: 6 }),
  check('visit_date', 'Provide valid visit date')
    .not()
    .isEmpty()
    .isDate()
    .isBefore(new Date().toString()),
  validationsHandler(),
];

module.exports.validateAddReplyInputs = [
  check('id', 'Please provide ID').not().isEmpty(),
  check('reply', 'Reply is required').not().isEmpty(),
  validationsHandler(),
];

module.exports.validateUpdateAuthUserInputs = [
  check('fullName', 'Full name is required').not().isEmpty(),
  body('oldPassword', 'Please provide current password')
    .if(body('password').not().isEmpty())
    .not()
    .isEmpty(),
  body('password', 'Password length should be 6 or more characters')
    .if(body('password').not().isEmpty())
    .isLength({
      min: 6,
    }),
  body('password2', 'Repeated password is not correct')
    .if(body('password').not().isEmpty())
    .custom((value, { req }) => value === req.body.password),
  validationsHandler(),
];

module.exports.validateDeleteInputs = [
  check('id', 'Please provide ID').not().isEmpty(),
  validationsHandler(),
];

module.exports.validateSignInInputs = [
  check('email', 'Please include a valid email').isEmail(),
  check('password', 'Password is required').not().isEmpty(),
  validationsHandler(),
];
