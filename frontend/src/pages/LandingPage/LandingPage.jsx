import React, { useContext, useEffect } from 'react';
import Button from '../../components/FormElements/Button';
import { AuthContext } from '../../shared/context/auth-context';

import './LandingPage.css';

const roleDescriptions = {
  admin: ['Edit/Delete Users, Restaurants, Reviews'],
  owner: [
    'Create/Edit Restaurants',
    'Reply to comments about owned restaurants',
  ],
  regular: ['See the list of restaurants', 'Rate and leave comment'],
};

const LandingPage = () => {
  const { userData, updateUserData } = useContext(AuthContext);
  useEffect(() => {
    updateUserData();
  }, [updateUserData]);
  const user = userData?.user;
  return (
    <section className="landing">
      <div className="dark-overlay">
        <div className="landing-inner">
          <h1 className="x-large capitalize">
            {user
              ? `Hello ${user.fullName.split(' ')[0]}`
              : 'Restaurant Reviewer'}
          </h1>
          <div className="lead">
            {!user ? (
              <div>
                Login as a regular user and see the best restaurants to visit
                <br />
                or
                <br />
                Sign up as an owner and add your restaurant for review
              </div>
            ) : (
              <div>
                You are logged in as {user.role}. You can: <br />
                {roleDescriptions[user.role].map(description => (
                  <div key={description}>
                    {description}
                    <br />
                  </div>
                ))}
                <div className="buttons">
                  <Button to="/list" color="light">
                    Restaurants List
                  </Button>
                  {user.role === 'admin' && (
                    <>
                      <Button to="/reviews-list" color="success">
                        Reviews List
                      </Button>
                      <Button to="/users-list" color="primary">
                        Users List
                      </Button>
                    </>
                  )}
                  {user.role === 'owner' && (
                    <>
                      <Button to="/add-new-restaurant" color="primary">
                        Add New Restaurant
                      </Button>
                      <Button to="/pending-reviews" color="warning">
                        Pending Reviews
                      </Button>
                    </>
                  )}
                </div>
              </div>
            )}
          </div>
          {!user && (
            <div className="buttons">
              <Button to="/login" color="light">
                Login
              </Button>
              <Button to="/register" color="primary">
                Sign Up
              </Button>
            </div>
          )}
        </div>
      </div>
    </section>
  );
};

export default LandingPage;
