import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import Input from '../../components/FormElements/Input';
import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import {
  VALIDATOR_MINLENGTH,
  VALIDATOR_REQUIRE,
} from '../../shared/util/validators';
import { useHttpClient } from '../../shared/hooks/http-hook';
import Button from '../../components/FormElements/Button';
import { UPDATE_AUTH_USER, GET_AUTH_USER } from '../../shared/enums/api';
import SuccessContainer from '../../components/FormElements/SuccessContainer';
import { useAuth } from '../../shared/hooks/auth-hook';

const EditProfile = () => {
  const { updateUserData } = useAuth();
  const { isLoading, error, send, clearError } = useHttpClient();
  const [success, setSuccess] = useState(null);
  const { register, handleSubmit, errors, setValue, getValues } = useForm();

  useEffect(() => {
    send(GET_AUTH_USER.URL, GET_AUTH_USER.METHOD)
      .then(response => {
        setValue('email', response.data.email);
        setValue('fullName', response.data.fullName);
      })
      .catch(err => err);
  }, [send, setValue]);

  const onSubmit = async data => {
    try {
      clearError();
      setSuccess(null);
      const { oldPassword, password, password2, fullName } = data;
      await send(UPDATE_AUTH_USER.URL, UPDATE_AUTH_USER.METHOD, {
        oldPassword,
        password,
        password2,
        fullName,
      });
      setSuccess('Changes has been saved successfully.');
      updateUserData();
    } catch (err) {}
  };

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {success && <SuccessContainer success={success} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <h1 className="large text-primary">Edit Profile</h1>
      <form className="form" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <Input
            element="input"
            name="email"
            type="email"
            label="E-Mail"
            disabled
            register={() => register}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="fullName"
            type="text"
            label="Full name"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Full name'),
              })
            }
            errorText={errors.fullName?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="oldPassword"
            type="password"
            label="Current Password"
            helperText="Current password is required if you are changing password"
            autoComplete="new-password"
            register={() => register}
            errorText={errors.oldPassword?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="password"
            type="password"
            label="New Password"
            autoComplete="new-password"
            register={() =>
              register({
                minLength: {
                  value: 6,
                  message: VALIDATOR_MINLENGTH('New password', 6),
                },
              })
            }
            errorText={errors.password?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="password2"
            type="password"
            label="Repeat New Password"
            autoComplete="new-password"
            register={() =>
              register({
                validate: value =>
                  getValues('password') === value || 'Does not match',
              })
            }
            errorText={errors.password2?.message}
          />
        </div>
        <Button type="submit" color="primary">
          Update
        </Button>
      </form>
    </section>
  );
};

export default EditProfile;
