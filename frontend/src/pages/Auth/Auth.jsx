import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import Input from '../../components/FormElements/Input';
import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import {
  VALIDATOR_EMAIL,
  VALIDATOR_REQUIRE,
  VALIDATOR_MINLENGTH,
} from '../../shared/util/validators';
import { useHttpClient } from '../../shared/hooks/http-hook';
import { AuthContext } from '../../shared/context/auth-context';
import Button from '../../components/FormElements/Button';
import { SIGN_IN } from '../../shared/enums/api';

const Auth = () => {
  const { login } = useContext(AuthContext);
  const { isLoading, error, send, clearError } = useHttpClient();
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = async data => {
    try {
      clearError();
      const { email, password } = data;
      const response = await send(SIGN_IN.URL, SIGN_IN.METHOD, {
        email,
        password,
      });

      login(response.token);
    } catch (err) {}
  };

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <h1 className="large text-primary">Sign In</h1>
      <p className="lead">
        <i className="fas fa-user" /> Sign into Your Account
      </p>
      <form className="form" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <Input
            element="input"
            name="email"
            type="email"
            label="E-Mail"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Email'),
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: VALIDATOR_EMAIL(),
                },
              })
            }
            errorText={errors.email?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="password"
            type="password"
            label="Password"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Password'),
                minLength: {
                  value: 6,
                  message: VALIDATOR_MINLENGTH('password', 6),
                },
              })
            }
            errorText={errors.password?.message}
          />
        </div>
        <Button type="submit" color="primary">
          Login
        </Button>
      </form>
      <p className="my-1">
        Dont have an account? <Link to="/register">Sign Up</Link>
      </p>
    </section>
  );
};

export default Auth;
