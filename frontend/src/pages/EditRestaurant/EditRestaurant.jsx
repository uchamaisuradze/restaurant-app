import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory, useParams } from 'react-router-dom';

import Input from '../../components/FormElements/Input';
import ImageUpload from '../../components/FormElements/ImageUpload';
import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import { VALIDATOR_REQUIRE } from '../../shared/util/validators';
import { useHttpClient } from '../../shared/hooks/http-hook';
import Button from '../../components/FormElements/Button';
import {
  GET_RESTAURANTS,
  STATIC_URL,
  UPDATE_RESTAURANT,
} from '../../shared/enums/api';
import SuccessContainer from '../../components/FormElements/SuccessContainer';

const EditRestaurant = () => {
  const { isLoading, error, statusCode, send, clearError } = useHttpClient();
  const {
    register,
    handleSubmit,
    errors,
    watch,
    getValues,
    setValue,
  } = useForm();
  const [success, setSuccess] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);
  const history = useHistory();
  const { id } = useParams();

  useEffect(() => {
    send(`${GET_RESTAURANTS.URL}?id=${id}`, GET_RESTAURANTS.METHOD)
      .then(response => {
        const { title, address, description, cover: image } = response.data;
        setValue('title', title);
        setValue('address', address);
        setValue('description', description);
        setPreviewImage(STATIC_URL + image);
      })
      .catch(err => err);
  }, [send, setValue, id]);

  watch('image');
  const imageFile = getValues('image');

  const onSubmit = async data => {
    try {
      clearError();
      setSuccess(null);

      const formData = new FormData();
      Object.keys(data).forEach(key => {
        if (key === 'image') {
          formData.append(key, data[key][0]);
        } else {
          formData.append(key, data[key]);
        }
      });
      await send(UPDATE_RESTAURANT.URL, UPDATE_RESTAURANT.METHOD, formData, {
        'Content-Type': 'multipart/form-data',
      });

      setSuccess('Changes has been saved successfully. Redirecting...');

      setTimeout(() => {
        history.push('/list');
      }, 2500);
    } catch (err) {}
  };

  if (error && statusCode === 404) {
    return (
      <section className="container">
        <ErrorContainer error={error} />
      </section>
    );
  }

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {success && <SuccessContainer success={success} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <h1 className="large text-primary">Edit Restaurant</h1>
      <form
        className="form"
        onSubmit={handleSubmit(onSubmit)}
        encType="multipart/form-data"
      >
        <input type="hidden" name="id" value={id} ref={register} />
        <div className="form-group">
          <Input
            element="input"
            name="title"
            type="text"
            label="Title *"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Title'),
              })
            }
            errorText={errors.title?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="address"
            type="text"
            label="Address"
            register={() => register}
          />
        </div>
        <div className="form-group">
          <Input
            element="textarea"
            name="description"
            label="Description *"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Description'),
              })
            }
            errorText={errors.description?.message}
          />
        </div>
        <div className="form-group">
          <ImageUpload
            center
            name="image"
            register={register}
            errorText={errors.image?.message}
            selectedFile={imageFile}
            previewImage={previewImage}
          />
        </div>
        <div className="line" />
        <Button to="/list" color="light">
          Back to the list
        </Button>
        <Button type="submit" color="primary" disabled={isLoading || !!success}>
          Update
        </Button>
      </form>
    </section>
  );
};

export default EditRestaurant;
