import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';

import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import { useHttpClient } from '../../shared/hooks/http-hook';
import { DELETE_RESTAURANT, GET_RESTAURANTS } from '../../shared/enums/api';
import Button from '../../components/FormElements/Button';
import { AuthContext } from '../../shared/context/auth-context';
import ListBox from '../../components/UIElements/ListBox';
import SuccessContainer from '../../components/FormElements/SuccessContainer';
import FilterContainer from '../../components/FilterContainer/FilterContainer';

const RestaurantsList = () => {
  const { isLoading, error, send, clearError } = useHttpClient();
  const [success, setSuccess] = useState(null);
  const [list, setList] = useState([]);
  const { userData } = useContext(AuthContext);
  const user = userData?.user;
  const [update, setUpdate] = useState(1);

  const { register, getValues, setValue, watch } = useForm();

  watch();

  const { sortBy, sortType, rateMin, rateMax } = getValues();

  const handleDelete = useCallback(
    restaurantId => async () => {
      clearError();
      setSuccess(null);
      try {
        if (window.confirm('Are you sure?')) {
          await send(DELETE_RESTAURANT.URL, DELETE_RESTAURANT.METHOD, {
            id: restaurantId,
          });
          setSuccess('Restaurant has been removed successfully.');
          setTimeout(() => {
            setSuccess(null);
          }, 2000);
          setUpdate(prevUpdate => -1 * prevUpdate);
        }
      } catch (e) {}
    },
    [setUpdate, clearError, send, setSuccess],
  );

  useEffect(() => {
    let query = {};
    if (sortBy && sortType) {
      query = {
        sortBy,
        sortType,
        rateMax,
        rateMin,
      };
    }

    const queryString = new URLSearchParams(query).toString();
    send(`${GET_RESTAURANTS.URL}?${queryString}`, GET_RESTAURANTS.METHOD)
      .then(response => {
        setList([...response.data]);
      })
      .catch(err => err);
  }, [send, setList, update, sortBy, sortType, rateMax, rateMin]);

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {success && <SuccessContainer success={success} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <div className="list-header">
        <h1 className="large text-primary">Restaurants List</h1>
        {user?.role === 'owner' && (
          <Button to="/add-new-restaurant" color="primary">
            Add New
          </Button>
        )}
      </div>
      <div className="sort-and-filter">
        <FilterContainer
          setValue={setValue}
          getValues={getValues}
          register={register}
        />
      </div>
      <div className="list">
        {list.map(record => (
          <ListBox
            id={record.id}
            key={record.id}
            cover={record.cover}
            title={record.title}
            createdAt={record.createdAt}
            averageRate={record.averageRate}
            role={user.role}
            handleDelete={handleDelete}
          />
        ))}
      </div>
    </section>
  );
};

export default RestaurantsList;
