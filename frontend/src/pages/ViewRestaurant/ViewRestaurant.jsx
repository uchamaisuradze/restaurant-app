import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import { useHttpClient } from '../../shared/hooks/http-hook';
import {
  DELETE_REVIEWS,
  GET_RESTAURANTS,
  STATIC_URL,
} from '../../shared/enums/api';
import { AuthContext } from '../../shared/context/auth-context';
import ReviewForm from '../../components/ReviewForm/ReviewForm';
import Button from '../../components/FormElements/Button';
import ReviewContainer from '../../components/ReviewContainer/ReviewContainer';
import RateInput from '../../components/FormElements/RateInput';

const ViewRestaurant = () => {
  const { isLoading, error, send, clearError } = useHttpClient();
  const [record, setRecord] = useState(null);
  const [update, setUpdate] = useState(1);
  const { userData } = useContext(AuthContext);
  const user = userData?.user;
  const { id } = useParams();

  useEffect(() => {
    send(`${GET_RESTAURANTS.URL}?id=${id}`, GET_RESTAURANTS.METHOD)
      .then(response => {
        setRecord({ ...response.data });
      })
      .catch(err => err);
  }, [send, id, update]);

  const handleDelete = useCallback(
    reviewId => async () => {
      clearError();
      try {
        if (window.confirm('Are you sure?')) {
          await send(DELETE_REVIEWS.URL, DELETE_REVIEWS.METHOD, {
            id: reviewId,
          });
          setUpdate(prevUpdate => -1 * prevUpdate);
        }
      } catch (e) {}
    },
    [setUpdate, clearError, send],
  );

  if (!record) {
    return (
      <section className="container">
        {error && <ErrorContainer error={error} />}
        {isLoading && <LoadingSpinner asOverlay />}
      </section>
    );
  }

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <Button to="/list" color="light">
        Back to the list
      </Button>

      <div className="restaurant-grid my-1">
        <div className="restaurant-top bg-primary p-2">
          <img className="my-1" src={STATIC_URL + record.cover} alt="" />
          <h1 className="large">{record.title}</h1>
          <p className="lead">
            {record.averageRate ? (
              <>
                Rate: {record.averageRate} <i className="fas fa-star" />
              </>
            ) : (
              'No Reviews'
            )}
          </p>
          <p>Address: {record.address}</p>
        </div>
        <div className="restaurant-about bg-light p-2">
          <h2 className="text-primary">Description</h2>
          <p>{record.description}</p>
        </div>

        {record.highestReview && (
          <div className="review-best bg-white p-2">
            <h2 className="text-primary">Highest Review</h2>
            <div>
              <h3 className="text-dark">
                {record.highestReview.user.fullName}
              </h3>
              <RateInput
                name="rate"
                min={1}
                max={5}
                value={record.highestReview.rate}
                disabled
              />{' '}
              <p>
                Visit Date:{' '}
                {new Date(record.highestReview.visitDate).toDateString()}
              </p>
              <p>{record.highestReview.comment}</p>
              {record.highestReview.reply && (
                <p>Reply: {record.highestReview.reply}</p>
              )}
            </div>
          </div>
        )}

        {record.lowestReview && (
          <div className="review-worst bg-white p-2">
            <h2 className="text-danger">Lowest Review</h2>
            <div>
              <h3 className="text-dark">{record.lowestReview.user.fullName}</h3>
              <RateInput
                name="rate"
                min={1}
                max={5}
                value={record.lowestReview.rate}
                disabled
              />{' '}
              <p>
                Visit Date:{' '}
                {new Date(record.lowestReview.visitDate).toDateString()}
              </p>
              <p>{record.lowestReview.comment}</p>
              {record.lowestReview.reply && (
                <p>Reply: {record.lowestReview.reply}</p>
              )}
            </div>
          </div>
        )}

        <div className="section-reviews">
          <h2 className="text-primary my-1">
            <i className="fa fa-comment" />{' '}
            {record.reviews.length > 0 ? 'Reviews' : 'No Reviews'}
          </h2>

          {record.reviews.map(review => (
            <ReviewContainer
              id={review.id}
              key={review.id}
              fullName={review.user.fullName}
              comment={review.comment}
              visitDate={review.visitDate}
              rate={review.rate}
              reply={review.reply}
              role={user.role}
              handleDelete={handleDelete}
              rerender={() => {
                setUpdate(-1 * update);
              }}
            />
          ))}

          <div className="line" />
          {record.canReview && (
            <ReviewForm
              restaurantId={id}
              rerender={() => {
                setUpdate(-1 * update);
              }}
            />
          )}
        </div>
      </div>
    </section>
  );
};

export default ViewRestaurant;
