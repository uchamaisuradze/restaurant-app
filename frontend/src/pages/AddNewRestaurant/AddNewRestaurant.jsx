import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

import Input from '../../components/FormElements/Input';
import ImageUpload from '../../components/FormElements/ImageUpload';
import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import { VALIDATOR_REQUIRE } from '../../shared/util/validators';
import { useHttpClient } from '../../shared/hooks/http-hook';
import Button from '../../components/FormElements/Button';
import { ADD_RESTAURANT } from '../../shared/enums/api';
import SuccessContainer from '../../components/FormElements/SuccessContainer';

const AddNewRestaurant = () => {
  const { isLoading, error, send, clearError } = useHttpClient();
  const { register, handleSubmit, errors, watch, getValues } = useForm();
  const [success, setSuccess] = useState(null);
  const history = useHistory();

  watch('image');
  const imageFile = getValues('image');

  const onSubmit = async data => {
    try {
      clearError();
      setSuccess(null);

      const formData = new FormData();
      Object.keys(data).forEach((key) => {
        if (key === 'image') {
          formData.append(key, data[key][0]);
        } else {
          formData.append(key, data[key]);
        }
      });
      await send(ADD_RESTAURANT.URL, ADD_RESTAURANT.METHOD, formData, {
        'Content-Type': 'multipart/form-data',
      });

      setSuccess('Restaurant has been added successfully. Redirecting...');

      setTimeout(() => {
        history.push('/list');
      }, 3000);
    } catch (err) {}
  };

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {success && <SuccessContainer success={success} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <h1 className="large text-primary">Add New Restaurant</h1>
      <form className="form" onSubmit={handleSubmit(onSubmit)} encType="multipart/form-data">
        <div className="form-group">
          <Input
            element="input"
            name="title"
            type="text"
            label="Title *"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Title'),
              })
            }
            errorText={errors.title?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="address"
            type="text"
            label="Address"
            register={() => register}
          />
        </div>
        <div className="form-group">
          <Input
            element="textarea"
            name="description"
            label="Description *"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Description'),
              })
            }
            errorText={errors.description?.message}
          />
        </div>
        <div className="form-group">
          <ImageUpload
            center
            name="image"
            register={
              register({
                required: VALIDATOR_REQUIRE('Image'),
              })
            }
            errorText={errors.image?.message}
            selectedFile={imageFile}
          />
        </div>
        <div className="line" />
        <Button type="submit" color="primary" disabled={isLoading || !!success}>
          Add
        </Button>
      </form>
    </section>
  );
};

export default AddNewRestaurant;
