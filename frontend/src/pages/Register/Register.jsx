import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from 'react-hook-form';

import Input from '../../components/FormElements/Input';
import Select from '../../components/FormElements/Select';
import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import {
  VALIDATOR_EMAIL,
  VALIDATOR_MINLENGTH,
  VALIDATOR_REQUIRE,
} from '../../shared/util/validators';
import { useHttpClient } from '../../shared/hooks/http-hook';
import { AuthContext } from '../../shared/context/auth-context';
import Button from '../../components/FormElements/Button';
import { SIGN_UP } from '../../shared/enums/api';

const Register = () => {
  const auth = useContext(AuthContext);
  const { isLoading, error, send, clearError } = useHttpClient();
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = async data => {
    try {
      clearError();
      const { email, password, fullName, role } = data;
      const response = await send(SIGN_UP.URL, SIGN_UP.METHOD, {
        email,
        password,
        fullName,
        role,
      });

      auth.login(response.token);
    } catch (err) {}
  };

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <h1 className="large text-primary">Sign up</h1>
      <form className="form" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <Input
            element="input"
            name="email"
            type="email"
            label="E-Mail"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Email'),
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: VALIDATOR_EMAIL(),
                },
              })
            }
            errorText={errors.email?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="password"
            type="password"
            label="Password"
            autoComplete="new-password"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Password'),
                minLength: {
                  value: 6,
                  message: VALIDATOR_MINLENGTH('password', 6),
                },
              })
            }
            errorText={errors.password?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="fullName"
            type="text"
            label="Full name"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Full name'),
              })
            }
            errorText={errors.fullName?.message}
          />
        </div>
        <div className="form-group">
          <Select
            name="role"
            label="Select Role"
            placeholder="..."
            options={[
              {
                title: 'Owner',
                value: 'owner',
              },
              {
                title: 'Regular user',
                value: 'regular',
              },
            ]}
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Role'),
              })
            }
            errorText={errors.role?.message}
          />
        </div>
        <Button type="submit" color="primary">
          Sign Up
        </Button>
      </form>
      <p className="my-1">
        Do you have an account? <Link to="/auth">Sign In</Link>
      </p>
    </section>
  );
};

export default Register;
