import React, { useCallback, useContext, useEffect, useState } from 'react';

import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import { useHttpClient } from '../../shared/hooks/http-hook';
import {
  DELETE_REVIEWS,
  GET_RESTAURANTS,
  GET_REVIEWS,
} from '../../shared/enums/api';
import { AuthContext } from '../../shared/context/auth-context';
import ReviewContainer from '../../components/ReviewContainer/ReviewContainer';

const ReviewsList = () => {
  const { isLoading, error, send, clearError } = useHttpClient();
  const [list, setList] = useState([]);
  const [update, setUpdate] = useState(1);
  const { userData } = useContext(AuthContext);
  const user = userData?.user;

  const handleDelete = useCallback(
    reviewId => async () => {
      clearError();
      try {
        if (window.confirm('Are you sure?')) {
          await send(DELETE_REVIEWS.URL, DELETE_REVIEWS.METHOD, {
            id: reviewId,
          });
          setUpdate(prevUpdate => -1 * prevUpdate);
        }
      } catch (e) {}
    },
    [setUpdate, clearError, send],
  );

  useEffect(() => {
    send(`${GET_REVIEWS.URL}`, GET_RESTAURANTS.METHOD)
      .then(response => {
        setList([...response.data]);
      })
      .catch(err => err);
  }, [send, setList, update]);

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      <div className="review-grid my-1">
        <div className="section-reviews">
          <h2 className="text-primary my-1">
            <i className="fa fa-comment" /> Reviews
          </h2>
          {isLoading && <LoadingSpinner asOverlay />}
          {list.map(review => (
            <ReviewContainer
              id={review.id}
              key={review.id}
              restaurant={review.restaurant.title}
              fullName={review.user.fullName}
              comment={review.comment}
              visitDate={review.visitDate}
              rate={review.rate}
              reply={review.reply}
              role={user.role}
              rerender={() => {
                setUpdate(-1 * update);
              }}
              handleDelete={handleDelete}
            />
          ))}
        </div>
      </div>
    </section>
  );
};

export default ReviewsList;
