import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';

import Input from '../../components/FormElements/Input';
import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import { VALIDATOR_REQUIRE } from '../../shared/util/validators';
import { useHttpClient } from '../../shared/hooks/http-hook';
import Button from '../../components/FormElements/Button';
import { UPDATE_REVIEWS, GET_REVIEWS } from '../../shared/enums/api';
import SuccessContainer from '../../components/FormElements/SuccessContainer';
import RateInput from '../../components/FormElements/RateInput';

const EditReview = () => {
  const { isLoading, error, send, clearError } = useHttpClient();
  const [success, setSuccess] = useState(null);
  const {
    register,
    handleSubmit,
    errors,
    setValue,
    watch,
    getValues,
  } = useForm();
  watch('rate');
  const { id } = useParams();

  useEffect(() => {
    send(`${GET_REVIEWS.URL}?id=${id}`, GET_REVIEWS.METHOD)
      .then(response => {
        setValue('rate', response.data.rate);
        setValue('visit_date', new Date(response.data.visitDate).toISOString().split('T')[0]);
        setValue('comment', response.data.comment);
        setValue('reply', response.data.reply);
      })
      .catch(err => err);
  }, [send, setValue, id]);

  const onSubmit = async data => {
    try {
      clearError();
      setSuccess(null);
      const { rate, reply, visit_date: visitDate, comment } = data;
      await send(UPDATE_REVIEWS.URL, UPDATE_REVIEWS.METHOD, {
        id,
        comment,
        visit_date: visitDate,
        reply,
        rate,
      });
      setSuccess('Changes has been saved successfully.');
    } catch (err) {}
  };

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {success && <SuccessContainer success={success} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <h1 className="large text-primary">Edit Review</h1>
      <form className="form" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group-row">
          <div className="form-group">
            <RateInput
              name="rate"
              label="Rate *"
              register={() =>
                register({
                  required: VALIDATOR_REQUIRE('Rate'),
                })
              }
              min={1}
              max={5}
              value={getValues('rate')}
              setValue={val => {
                setValue('rate', val);
              }}
              errorText={errors.rate?.message}
            />
          </div>
          <div className="form-group">
            <Input
              element="input"
              name="visit_date"
              type="date"
              label="Visit Date *"
              max={new Date().toISOString().split('T')[0]}
              register={() =>
                register({
                  required: VALIDATOR_REQUIRE('Visit Date'),
                })
              }
            />
          </div>
        </div>
        <div className="form-group">
          <Input
            element="textarea"
            name="comment"
            label="Comment *"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Comment'),
              })
            }
            errorText={errors.comment?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="textarea"
            name="reply"
            label="Reply"
            register={() => register()}
            errorText={errors.reply?.message}
          />
        </div>
        <div className="line" />
        <Button to="/reviews-list" color="light">
          Back to the list
        </Button>
        <Button type="submit" color="primary" disabled={isLoading}>
          Update
        </Button>
      </form>
    </section>
  );
};

export default EditReview;
