import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';

import Input from '../../components/FormElements/Input';
import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import {
  VALIDATOR_EMAIL,
  VALIDATOR_MINLENGTH,
  VALIDATOR_REQUIRE,
} from '../../shared/util/validators';
import { useHttpClient } from '../../shared/hooks/http-hook';
import Button from '../../components/FormElements/Button';
import { UPDATE_USER, GET_USERS } from '../../shared/enums/api';
import SuccessContainer from '../../components/FormElements/SuccessContainer';
import { useAuth } from '../../shared/hooks/auth-hook';

const EditUser = () => {
  const { updateUserData } = useAuth();
  const { isLoading, error, send, clearError } = useHttpClient();
  const [success, setSuccess] = useState(null);
  const { register, handleSubmit, errors, setValue } = useForm();
  const { id } = useParams();

  useEffect(() => {
    send(`${GET_USERS.URL}?id=${id}`, GET_USERS.METHOD)
      .then(response => {
        setValue('email', response.data.email);
        setValue('fullName', response.data.fullName);
      })
      .catch(err => err);
  }, [send, setValue, id]);

  const onSubmit = async data => {
    try {
      clearError();
      setSuccess(null);
      const { password, fullName, email } = data;
      await send(UPDATE_USER.URL, UPDATE_USER.METHOD, {
        id,
        password,
        email,
        fullName,
      });
      setSuccess('Changes has been saved successfully.');
      updateUserData();
    } catch (err) {}
  };

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {success && <SuccessContainer success={success} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <h1 className="large text-primary">Edit User</h1>
      <form className="form" onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <Input
            element="input"
            name="email"
            type="email"
            label="E-Mail"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Email'),
                pattern: {
                  value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                  message: VALIDATOR_EMAIL(),
                },
              })
            }
            errorText={errors.email?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="fullName"
            type="text"
            label="Full name"
            register={() =>
              register({
                required: VALIDATOR_REQUIRE('Full name'),
              })
            }
            errorText={errors.fullName?.message}
          />
        </div>
        <div className="form-group">
          <Input
            element="input"
            name="password"
            type="password"
            label="New Password"
            autoComplete="new-password"
            register={() =>
              register({
                minLength: {
                  value: 6,
                  message: VALIDATOR_MINLENGTH('New password', 6),
                },
              })
            }
            errorText={errors.password?.message}
          />
        </div>
        <div className="line" />
        <Button to="/users-list" color="light">
          Back to the list
        </Button>
        <Button type="submit" color="primary" disabled={isLoading}>
          Update
        </Button>
      </form>
    </section>
  );
};

export default EditUser;
