import React, { useEffect, useState, useCallback } from 'react';

import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import { useHttpClient } from '../../shared/hooks/http-hook';
import { DELETE_USER, GET_USERS } from '../../shared/enums/api';
import UserListBox from '../../components/UIElements/UserListBox';
import SuccessContainer from '../../components/FormElements/SuccessContainer';

const UsersList = () => {
  const { isLoading, error, send, clearError } = useHttpClient();
  const [success, setSuccess] = useState(null);
  const [list, setList] = useState([]);
  const [update, setUpdate] = useState(1);

  useEffect(() => {
    send(GET_USERS.URL, GET_USERS.METHOD)
      .then(response => {
        setList([...response.data]);
      })
      .catch(err => err);
  }, [send, setList, update]);

  const handleDelete = useCallback(
    userId => async () => {
      clearError();
      setSuccess(null);
      try {
        if (
          window.confirm('Are you sure? All data under this user will be lost')
        ) {
          await send(DELETE_USER.URL, DELETE_USER.METHOD, {
            id: userId,
          });
          setSuccess('User has been removed successfully.');
          setTimeout(() => {
            setSuccess(null);
          }, 2000);
          setUpdate(prevUpdate => -1 * prevUpdate);
        }
      } catch (e) {}
    },
    [setUpdate, clearError, send, setSuccess],
  );

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {success && <SuccessContainer success={success} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <div className="list-header">
        <h1 className="large text-primary">Users List</h1>
      </div>
      <div className="list">
        {list.map(record => (
          <UserListBox
            id={record.id}
            key={record.id}
            fullName={record.fullName}
            role={record.role}
            createdAt={record.createdAt}
            handleDelete={handleDelete}
          />
        ))}
      </div>
    </section>
  );
};

export default UsersList;
