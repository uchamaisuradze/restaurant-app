import React, { useContext, useEffect, useState } from 'react';

import ErrorContainer from '../../components/FormElements/ErrorContainer';
import LoadingSpinner from '../../components/UIElements/LoadingSpinner';
import { useHttpClient } from '../../shared/hooks/http-hook';
import { GET_RESTAURANTS, GET_REVIEWS } from '../../shared/enums/api';
import { AuthContext } from '../../shared/context/auth-context';
import ReviewContainer from '../../components/ReviewContainer/ReviewContainer';

const PendingReviews = () => {
  const { isLoading, error, send } = useHttpClient();
  const [list, setList] = useState([]);
  const [update, setUpdate] = useState(1);
  const { userData } = useContext(AuthContext);
  const user = userData?.user;

  useEffect(() => {
    send(`${GET_REVIEWS.URL}?pending`, GET_RESTAURANTS.METHOD)
      .then(response => {
        setList([...response.data]);
      })
      .catch(err => err);
  }, [send, setList, update]);

  return (
    <section className="container">
      {error && <ErrorContainer error={error} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <div className="review-grid my-1">
        <div className="section-reviews">
          <h2 className="text-primary my-1">
            <i className="fa fa-comment" />{' '}
            {list.length > 0 ? 'Pending Reply' : 'All Reviews are replied'}
          </h2>

          {list.map(review => (
            <ReviewContainer
              id={review.id}
              key={review.id}
              restaurant={review.restaurant.title}
              fullName={review.user.fullName}
              comment={review.comment}
              visitDate={review.visitDate}
              rate={review.rate}
              reply={review.reply}
              role={user.role}
              rerender={() => {
                setUpdate(-1 * update);
              }}
            />
          ))}
        </div>
      </div>
    </section>
  );
};

export default PendingReviews;
