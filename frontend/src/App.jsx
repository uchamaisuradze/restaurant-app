import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from 'react-router-dom';

import { AuthContext } from './shared/context/auth-context';
import { useAuth } from './shared/hooks/auth-hook';
import Header from './components/Header/Header';
import LoadingSpinner from './components/UIElements/LoadingSpinner';
import LandingPage from './pages/LandingPage/LandingPage';
import Auth from './pages/Auth/Auth';
import Register from './pages/Register/Register';
import EditProfile from './pages/EditProfile/EditProfile';
import AddNewRestaurant from './pages/AddNewRestaurant/AddNewRestaurant';
import RestaurantsList from './pages/RestaurantsList/RestaurantsList';
import EditRestaurant from './pages/EditRestaurant/EditRestaurant';
import ViewRestaurant from './pages/ViewRestaurant/ViewRestaurant';
import UsersList from './pages/UsersList/UsersList';
import EditUser from './pages/EditUser/EditUser';
import PendingReviews from './pages/PendingReviews/PendingReviews';
import ReviewsList from './pages/ReviewsList/ReviewsList';
import EditReview from './pages/EditReview/EditReview';

const App = () => {
  const { token, login, logout, userData, updateUserData } = useAuth();
  let routes;
  if (token) {
    if (!userData) {
      return <LoadingSpinner asOverlay />;
    }
    const { role } = userData.user;
    routes = (
      <Switch>
        <Route path="/" exact>
          <LandingPage />
        </Route>
        <Route path="/edit-profile" exact>
          <EditProfile />
        </Route>
        {role === 'admin' && (
          <Route path="/edit-user/:id" exact>
            <EditUser />
          </Route>
        )}
        {role === 'admin' && (
          <Route path="/edit-review/:id" exact>
            <EditReview />
          </Route>
        )}
        {role === 'admin' && (
          <Route path="/users-list" exact>
            <UsersList />
          </Route>
        )}
        {role === 'admin' && (
          <Route path="/reviews-list" exact>
            <ReviewsList />
          </Route>
        )}
        {role === 'owner' && (
          <Route path="/add-new-restaurant" exact>
            <AddNewRestaurant />
          </Route>
        )}
        {role === 'owner' && (
          <Route path="/pending-reviews" exact>
            <PendingReviews />
          </Route>
        )}
        {role !== 'regular' && (
          <Route path="/edit-restaurant/:id" exact>
            <EditRestaurant />
          </Route>
        )}
        <Route path="/view-restaurant/:id" exact>
          <ViewRestaurant />
        </Route>
        <Route path="/list" exact>
          <RestaurantsList />
        </Route>
        <Redirect to="/" />
      </Switch>
    );
  } else {
    routes = (
      <Switch>
        <Route path="/" exact>
          <LandingPage />
        </Route>
        <Route path="/auth">
          <Auth />
        </Route>
        <Route path="/register">
          <Register />
        </Route>
        <Redirect to="/auth" />
      </Switch>
    );
  }

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn: !!token,
        token,
        userData,
        updateUserData,
        login,
        logout,
      }}
    >
      <Router>
        <Header userData={userData} logout={logout} />
        <main>{routes}</main>
      </Router>
    </AuthContext.Provider>
  );
};

export default App;
