import React from 'react';
import RateInput from '../FormElements/RateInput';

const FilterContainer = ({ setValue, getValues, register }) => (
  <>
    <div className="form-control">
      <span>Sort By:</span>
      <div className="list-filter">
        <select name="sortBy" id="sortBy" ref={register}>
          <option value="rating">Average Rating</option>
          <option value="date">Date</option>
        </select>
        <select name="sortType" id="sortType" ref={register}>
          <option value="desc">Descending</option>
          <option value="asc">Ascending</option>
        </select>
      </div>
    </div>
    <div className="form-control">
      <span>Filter By Rating:</span>
      <div className="list-filter">
        <div className="form-group">
          <RateInput
            name="rateMin"
            label="From"
            register={() => register()}
            min={1}
            max={5}
            value={getValues('rateMin')}
            setValue={val => {
              setValue('rateMin', val);
            }}
          />
        </div>
        <div className="form-group">
          <RateInput
            name="rateMax"
            label="To"
            register={() => register()}
            min={1}
            max={5}
            value={getValues('rateMax')}
            setValue={val => {
              setValue('rateMax', val);
            }}
          />
        </div>
      </div>
    </div>
  </>
);

export default FilterContainer;
