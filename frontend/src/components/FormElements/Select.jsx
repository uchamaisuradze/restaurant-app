import React from 'react';

import './Select.css';

const Select = ({ name, defaultValue = '', options, placeholder, label, errorText, register }) => (
  <div className={`form-control ${errorText ? 'form-control--invalid' : ''}`}>
    <label htmlFor={name}>{label}</label>
    <select
      name={name}
      ref={register()}
      defaultValue={defaultValue}
    >
      <option value="" disabled>
        {placeholder}
      </option>
      {options.map(opt => (
        <option value={opt.value} key={opt.value}>
          {opt.title}
        </option>
      ))}
    </select>
    {errorText && <p>{errorText}</p>}
  </div>
);

export default Select;
