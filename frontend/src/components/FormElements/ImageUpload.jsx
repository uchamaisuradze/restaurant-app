import React, { useRef, useState, useEffect } from 'react';

import Button from './Button';
import './ImageUpload.css';

const ImageUpload = ({ name, register, errorText, selectedFile, previewImage = null }) => {
  const [previewUrl, setPreviewUrl] = useState();
  const fileInputRef = useRef();

  useEffect(() => {
    if (!selectedFile || selectedFile.length === 0) {
      setPreviewUrl(previewImage);
      return;
    }
    const fileReader = new FileReader();
    fileReader.onload = () => {
      setPreviewUrl(fileReader.result);
    };
    fileReader.readAsDataURL(selectedFile[0]);
  }, [selectedFile, previewImage]);
  return (
    <div className={`form-control ${errorText && 'form-control--invalid'}`}>
      <input
        id={name}
        name={name}
        ref={ref => {
          fileInputRef.current = ref;
          register(ref);
        }}
        style={{ display: 'none' }}
        type="file"
        accept=".jpg,.png,.jpeg"
      />
      <div className="image-upload">
        <div className="image-upload__preview">
          {(previewUrl || previewImage) && <img src={previewUrl || previewImage} alt="Preview" />}
          {!previewUrl && !previewImage && <p>Please upload image</p>}
        </div>
        <Button type="button" color="dark" onClick={() => fileInputRef.current?.click()}>
          Choose image
        </Button>
      </div>
      {errorText && <p>{errorText}</p>}
    </div>
  );
};

export default ImageUpload;
