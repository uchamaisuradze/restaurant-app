import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import './Button.css';

const Button = ({
  children,
  to,
  exact,
  onClick,
  disabled,
  type,
  color,
  size,
  style,
}) =>
  to ? (
    <Link
      to={to}
      exact={exact}
      style={style}
      className={`btn btn-${color} ${size ? `btn-${size}` : ''}`}
    >
      {children}
    </Link>
  ) : (
    <button
      className={`btn btn-${color} ${size ? `btn-${size}` : ''}`}
      type={type}
      onClick={onClick}
      disabled={disabled}
      style={style}
    >
      {children}
    </button>
  );

Button.defaultProps = {
  children: null,
  to: null,
  exact: 'false',
  onClick: () => {},
  disabled: false,
  type: null,
  color: 'primary',
};

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  to: PropTypes.string,
  exact: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  color: PropTypes.oneOf([
    'primary',
    'light',
    'dark',
    'danger',
    'warning',
    'success',
    'white',
  ]),
};

export default Button;
