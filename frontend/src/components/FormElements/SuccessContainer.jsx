import React from 'react';

const SuccessContainer = ({ success }) => (
  <div className="alert alert-success">
    <p>{success}</p>
  </div>
);

export default SuccessContainer;
