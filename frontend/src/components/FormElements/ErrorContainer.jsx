import React from 'react';

const ErrorContainer = ({ error }) => (
  <div className="alert alert-danger">
    {error.split('\n').map(value => (
      <p key={value}>{value}</p>
    ))}
  </div>
);

export default ErrorContainer;
