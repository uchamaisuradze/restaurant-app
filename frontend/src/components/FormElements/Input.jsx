import React from 'react';

import './Input.css';

const Input = ({
  name,
  type,
  element: propsElement,
  placeholder,
  rows,
  label,
  errorText,
  helperText,
  register,
  disabled = false,
  autoComplete,
  max,
}) => {
  const element =
    propsElement === 'input' ? (
      <input
        id={name}
        name={name}
        type={type}
        placeholder={placeholder}
        ref={register()}
        disabled={disabled}
        autoComplete={autoComplete}
        max={max}
      />
    ) : (
      <textarea
        id={name}
        name={name}
        rows={rows || 3}
        placeholder={placeholder}
        ref={register()}
        disabled={disabled}
      />
    );

  return (
    <div className={`form-control ${errorText ? 'form-control--invalid' : ''}`}>
      <label htmlFor={name}>{label}</label>
      {element}
      {helperText && <small className="form-text">{helperText}</small>}
      {errorText && <p>{errorText}</p>}
    </div>
  );
};

export default Input;
