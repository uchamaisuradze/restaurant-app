import React, { useCallback, useState } from 'react';

import './Input.css';

const RateInput = ({
  name,
  label,
  errorText,
  register = null,
  value,
  setValue,
  disabled = false,
  max,
  min,
}) => {
  const [stars, setStars] = useState(
    Array.from(Array(max), (_, index) => ({ value: index + 1, active: false })),
  );
  const mouseOverHandler = useCallback(
    index => () => {
      setStars(prevStars => {
        const newStars = [];
        prevStars.forEach((prevStar, localIndex) => {
          if (localIndex <= index) {
            newStars.push({ ...prevStar, active: true });
          } else {
            newStars.push({ ...prevStar, active: false });
          }
        });
        return newStars;
      });
    },
    [setStars],
  );
  const mouseOutHandler = useCallback(() => {
    setStars(prevStars =>
      prevStars.map(prevStar => ({ ...prevStar, active: false })),
    );
  }, []);
  const onClickHandler = useCallback(
    index => () => {
      if (Number(value) === stars[index].value) {
        setValue(null);
      } else {
        setValue(stars[index].value);
      }
    },
    [setValue, stars, value],
  );

  return (
    <>
      {!disabled && register && (
        <div
          className={`form-control ${errorText ? 'form-control--invalid' : ''}`}
        >
          <label htmlFor={name}>{label}</label>
          <div className="rate-input" onMouseOut={mouseOutHandler}>
            {stars.map((star, index) => (
              <div
                onMouseOver={mouseOverHandler(index)}
                key={star.value}
                onClick={onClickHandler(index)}
              >
                <i
                  className={`${
                    star.active || (value && star.value <= value)
                      ? 'fas'
                      : 'far'
                  } fa-star`}
                />
              </div>
            ))}
          </div>
          <input
            id={name}
            name={name}
            type="hidden"
            ref={register()}
            disabled={disabled}
            max={max}
            min={min}
          />
          {errorText && <p>{errorText}</p>}
        </div>
      )}
      {disabled && !register && (
        <div className="rate-input" style={{ alignItems: 'flex-end' }}>
          {stars.map(star => (
            <div key={star.value}>
              <i
                className={`${
                  value && star.value <= value ? 'fas' : 'far'
                } fa-star`}
              />
            </div>
          ))}
          <div style={{ fontSize: '18px' }}>- {value}</div>
        </div>
      )}
    </>
  );
};

export default RateInput;
