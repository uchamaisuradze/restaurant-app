import React from 'react';
import { STATIC_URL } from '../../shared/enums/api';
import Button from '../FormElements/Button';

const ListBox = ({
  id,
  cover,
  title,
  createdAt,
  role,
  averageRate,
  handleDelete = () => {},
}) => (
  <div key={id} className="list-box bg-light">
    <div className="list-box-image">
      {cover && <img src={STATIC_URL + cover} alt="" />}
    </div>

    <div>
      <h2>{title}</h2>
      <p>
        {averageRate ? (
          <>
            <i className="fas fa-star" /> {averageRate}
          </>
        ) : (
          'No Reviews'
        )}
      </p>
      <p>Created at: {new Date(createdAt).toDateString()}</p>
      <div className="line" />
      {role === 'admin' && (
        <Button onClick={handleDelete(id)} color="danger">
          Delete
        </Button>
      )}
      {role !== 'regular' && (
        <Button to={`/edit-restaurant/${id}`} color="white">
          Edit
        </Button>
      )}
      <Button to={`/view-restaurant/${id}`} className="btn btn-dark">
        View
      </Button>
    </div>
  </div>
);

export default ListBox;
