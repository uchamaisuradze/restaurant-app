import React from 'react';
import Button from '../FormElements/Button';

const UserListBox = ({ id, fullName, createdAt, role, handleDelete }) => (
  <div key={id} className="list-box user-list bg-light">
    <div>
      <h2>{fullName}</h2>
      <p>Created at: {new Date(createdAt).toDateString()}</p>
      <p>Role: {role.toUpperCase()}</p>
      <div className="line" />
      <Button onClick={handleDelete(id)} color="danger">
        Delete
      </Button>
      <Button to={`/edit-user/${id}`} color="white">
        Edit
      </Button>
    </div>
  </div>
);

export default UserListBox;
