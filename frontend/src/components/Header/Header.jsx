import React from 'react';
import { Link } from 'react-router-dom';

import './Header.css';
import Button from '../FormElements/Button';

const Header = ({ userData, logout }) => (
  <nav className="navbar bg-dark">
    <h1>
      <Link to="/">
        <i className="fas fa-utensils" /> Restaurants Reviewer
      </Link>
    </h1>
    <ul>
      {userData ? (
        <>
          {userData.user.role === 'admin' && (
            <li>
              <Link to="/reviews-list">Reviews List</Link>
            </li>
          )}
          {userData.user.role === 'admin' && (
            <li>
              <Link to="/users-list">Users List</Link>
            </li>
          )}
          {userData.user.role === 'owner' && (
            <li>
              <Link to="/pending-reviews">Pending Reviews</Link>
            </li>
          )}
          <li>
            <Link to="/list">Restaurants List</Link>
          </li>
          <li>|</li>
          <li>
            <Link to="/edit-profile">Edit Profile</Link>
          </li>
          <li>
            <Button onClick={logout} size="sm">
              Logout
            </Button>
          </li>
        </>
      ) : (
        <>
          <li>
            <Link to="/auth">Sign in</Link>
          </li>
          <li>
            <Link to="/register">Sign up</Link>
          </li>
        </>
      )}
    </ul>
  </nav>
);

export default Header;
