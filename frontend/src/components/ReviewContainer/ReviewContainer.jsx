import React, { useCallback } from 'react';
import { useForm } from 'react-hook-form';

import Button from '../FormElements/Button';
import { useHttpClient } from '../../shared/hooks/http-hook';
import { ADD_REPLY_TO_REVIEW } from '../../shared/enums/api';
import ErrorContainer from '../FormElements/ErrorContainer';
import LoadingSpinner from '../UIElements/LoadingSpinner';
import Input from '../FormElements/Input';
import { VALIDATOR_REQUIRE } from '../../shared/util/validators';
import RateInput from '../FormElements/RateInput';

const ReviewContainer = ({
  id,
  role,
  fullName,
  comment,
  visitDate,
  rate,
  reply,
  rerender,
  restaurant,
  handleDelete = () => {},
}) => {
  const { isLoading, error, send, clearError } = useHttpClient();

  const { register, handleSubmit, errors } = useForm();

  const onSubmit = useCallback(
    async data => {
      try {
        clearError();
        const { reply: replyInput } = data;
        await send(ADD_REPLY_TO_REVIEW.URL, ADD_REPLY_TO_REVIEW.METHOD, {
          id,
          reply: replyInput,
        });
        rerender();
      } catch (err) {}
    },
    [send, id, clearError, rerender],
  );

  return (
    <>
      {error && <ErrorContainer error={error} />}
      {isLoading && <LoadingSpinner asOverlay />}
      <div className="repo bg-white p-1 my-1">
        <div>
          <RateInput name="rate" min={1} max={5} value={rate} disabled />{' '}
          {restaurant && <div>{restaurant}</div>}
          <h4>
            <span className="text-primary">{fullName}</span> - Visit Date:{' '}
            {new Date(visitDate).toDateString()}
          </h4>
          <p>{comment}</p>
          {reply && (
            <>
              <div className="line" />
              <p>Reply: {reply}</p>
            </>
          )}
          {!reply && role === 'owner' && (
            <>
              <div className="line" />
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-group">
                  <Input
                    element="textarea"
                    name="reply"
                    placeholder="Your reply..."
                    register={() =>
                      register({
                        required: VALIDATOR_REQUIRE('Reply'),
                      })
                    }
                    errorText={errors.reply?.message}
                  />
                </div>
                <Button
                  type="submit"
                  style={{ float: 'right' }}
                  color="primary"
                  disabled={isLoading}
                >
                  Add Reply
                </Button>
              </form>
            </>
          )}
          {role === 'admin' && (
            <>
              <div className="line" />
              <Button
                onClick={handleDelete(id)}
                style={{ float: 'right' }}
                color="danger"
              >
                Delete
              </Button>
              <Button
                to={`/edit-review/${id}`}
                style={{ float: 'right' }}
                color="primary"
              >
                Edit Review
              </Button>
            </>
          )}
        </div>
      </div>
    </>
  );
};

export default ReviewContainer;
