import React, { useCallback, useState } from 'react';
import { useForm } from 'react-hook-form';

import { VALIDATOR_REQUIRE } from '../../shared/util/validators';
import Input from '../FormElements/Input';
import Button from '../FormElements/Button';
import ErrorContainer from '../FormElements/ErrorContainer';
import LoadingSpinner from '../UIElements/LoadingSpinner';
import SuccessContainer from '../FormElements/SuccessContainer';
import { useHttpClient } from '../../shared/hooks/http-hook';
import { ADD_REVIEW } from '../../shared/enums/api';
import RateInput from '../FormElements/RateInput';

const ReviewForm = ({ restaurantId, rerender }) => {
  const { isLoading, error, send, clearError } = useHttpClient();

  const { register, handleSubmit, errors, getValues, setValue, watch } = useForm();
  const [success, setSuccess] = useState(null);

  const onSubmit = useCallback(
    async data => {
      try {
        clearError();
        const { rate, visit_date: visitDate, comment } = data;
        await send(ADD_REVIEW.URL, ADD_REVIEW.METHOD, {
          rate,
          visit_date: visitDate,
          comment,
          restaurant_id: restaurantId,
        });
        setSuccess('Review has been added successfully.');
        setTimeout(() => {
          rerender();
          setSuccess(null);
        }, 1500);
      } catch (err) {}
    },
    [setSuccess, send, restaurantId, clearError, rerender],
  );

  watch('rate');

  return (
    <>
      {error && <ErrorContainer error={error} />}
      {isLoading && <LoadingSpinner asOverlay />}
      {success ? (
        <SuccessContainer success={success} />
      ) : (
        <div className="repo bg-white p-1 my-1">
          <div>
            <h4>
              <span>
                <i className="far fa-comment-dots" /> Have you ever been here?
                Let us know
              </span>
            </h4>
            <form onSubmit={handleSubmit(onSubmit)}>
              <div className="form-group-row">
                <div className="form-group">
                  <RateInput
                    name="rate"
                    label="Rate *"
                    register={() =>
                      register({
                        required: VALIDATOR_REQUIRE('Rate'),
                      })
                    }
                    min={1}
                    max={5}
                    value={getValues('rate')}
                    setValue={val => {
                      setValue('rate', val);
                    }}
                    errorText={errors.rate?.message}
                  />
                </div>
                <div className="form-group">
                  <Input
                    element="input"
                    name="visit_date"
                    type="date"
                    label="Visit Date *"
                    max={new Date().toISOString().split('T')[0]}
                    register={() =>
                      register({
                        required: VALIDATOR_REQUIRE('Visit Date'),
                      })
                    }
                  />
                </div>
              </div>
              <div className="form-group">
                <Input
                  element="textarea"
                  name="comment"
                  label="Comment *"
                  register={() =>
                    register({
                      required: VALIDATOR_REQUIRE('Comment'),
                    })
                  }
                  errorText={errors.comment?.message}
                />
              </div>
              <Button
                type="submit"
                color="primary"
                disabled={isLoading || !!success}
              >
                Add Review
              </Button>
            </form>
          </div>
        </div>
      )}
    </>
  );
};

export default ReviewForm;
