export const BASE_URL = 'http://localhost:5000/api/';
export const STATIC_URL = 'http://localhost:5000/';

export const SIGN_IN = { URL: '/auth', METHOD: 'POST' };
export const GET_AUTH_USER = { URL: '/auth', METHOD: 'GET' };
export const UPDATE_AUTH_USER = { URL: '/auth', METHOD: 'PATCH' };

export const SIGN_UP = { URL: '/users', METHOD: 'POST' };
export const GET_USERS = { URL: '/users', METHOD: 'GET' };
export const UPDATE_USER = { URL: '/users', METHOD: 'PATCH' };
export const DELETE_USER = { URL: '/users', METHOD: 'DELETE' };

export const ADD_RESTAURANT = { URL: '/restaurants', METHOD: 'POST' };
export const GET_RESTAURANTS = { URL: '/restaurants', METHOD: 'GET' };
export const UPDATE_RESTAURANT = { URL: '/restaurants', METHOD: 'PATCH' };
export const DELETE_RESTAURANT = { URL: '/restaurants', METHOD: 'DELETE' };

export const ADD_REVIEW = { URL: '/reviews', METHOD: 'POST' };
export const GET_REVIEWS = { URL: '/reviews', METHOD: 'GET' };
export const UPDATE_REVIEWS = { URL: '/reviews', METHOD: 'PATCH' };
export const DELETE_REVIEWS = { URL: '/reviews', METHOD: 'DELETE' };
export const ADD_REPLY_TO_REVIEW = { URL: '/reviews/reply', METHOD: 'POST' };
