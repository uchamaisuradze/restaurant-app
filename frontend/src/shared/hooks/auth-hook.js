import { useState, useCallback, useEffect } from 'react';
import { GET_AUTH_USER } from '../enums/api';
import { useHttpClient } from './http-hook';

export const useAuth = () => {
  const { send } = useHttpClient();
  const [token, setToken] = useState(localStorage.getItem('token'));
  const [userData, setUserData] = useState(null);

  const parseTokenAndSaveData = useCallback(() => {
    const storedData = localStorage.getItem('token');
    if (storedData) {
      try {
        const payload = JSON.parse(atob(storedData.split('.')[1]));
        setUserData(payload);
        return payload;
      } catch (error) {}
    }
    setUserData(null);
    return null;
  }, []);

  const login = useCallback(
    jwtToken => {
      setToken(jwtToken);
      localStorage.setItem('token', jwtToken);
      parseTokenAndSaveData();
    },
    [parseTokenAndSaveData],
  );

  const logout = useCallback(() => {
    setToken(null);
    localStorage.removeItem('token');
    parseTokenAndSaveData();
  }, [parseTokenAndSaveData]);

  const updateUserData = useCallback(() => {
    const storedData = localStorage.getItem('token');
    if (storedData) {
      login(storedData);
      send(
        GET_AUTH_USER.URL,
        GET_AUTH_USER.METHOD,
        {},
        {
          Authorization: `Bearer ${storedData}`,
        },
      )
        .then(response => {
          const { id, fullName, role } = response.data;
          setUserData(prevState => ({
            ...prevState,
            user: { id, fullName, role },
          }));
        })
        .catch(err => {
          if (err) {
            logout();
          }
        });
    }
  }, [login, setUserData, send, logout]);

  useEffect(() => {
    updateUserData();
  }, [updateUserData]);

  return { token, login, logout, userData, updateUserData };
};
