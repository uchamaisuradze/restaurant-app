import { useState, useCallback, useContext } from 'react';
import axios from 'axios';
import { BASE_URL } from '../enums/api';
import { AuthContext } from '../context/auth-context';

export const useHttpClient = () => {
  const token = localStorage.getItem('token');
  const { updateUserData } = useContext(AuthContext);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const [statusCode, setStatusCode] = useState();

  const send = useCallback(
    async (
      url,
      method = 'GET',
      data = null,
      headerParams = null,
      baseURL = BASE_URL,
    ) => {
      setIsLoading(true);

      let headers = {};
      if (headerParams) {
        headers = { ...headerParams };
      }
      if (token) {
        headers.Authorization = `Bearer ${token}`;
      }

      try {
        const response = await axios({
          url,
          baseURL,
          method,
          data,
          headers,
          timeout: 10000,
        });

        setIsLoading(false);
        return response.data;
      } catch (err) {
        let errMessage = err.message;
        if (err.response?.data?.errors?.length > 0) {
          errMessage = err.response.data.errors.reduce(
            (current, previous, index) =>
              index === 0 ? previous.msg : `${current}\n${previous.msg}`,
            '',
          );
        }
        setError(errMessage);
        setStatusCode(err.response.status);
        setIsLoading(false);
        if (token && err.response.status === 401) {
          updateUserData();
        }
        throw err;
      }
    },
    [token, updateUserData],
  );

  const clearError = () => {
    setError(null);
    setStatusCode(null);
  };

  return { isLoading, error, statusCode, send, clearError };
};
