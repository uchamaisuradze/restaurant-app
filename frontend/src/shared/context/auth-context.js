import { createContext } from 'react';

export const AuthContext = createContext({
  isLoggedIn: false,
  userData: null,
  token: null,
  updateUserData: () => {},
  login: () => {},
  logout: () => {},
});
