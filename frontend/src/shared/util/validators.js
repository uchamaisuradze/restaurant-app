export const VALIDATOR_REQUIRE = key => `${key} is required`;
export const VALIDATOR_EMAIL = () => 'Please enter a valid email address';
export const VALIDATOR_MINLENGTH = (key, length) => `Please enter a valid ${key}, at least ${length} characters`;
export const VALIDATOR_MAXLENGTH = (key, length) => `Please enter a valid ${key}, no more than ${length} characters`;
